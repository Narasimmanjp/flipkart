package testcases;

import java.awt.Desktop.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.RegEx;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC001Test1 {
	public static ChromeDriver driver;
	@BeforeMethod
	public void Preset() {
		  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			//		Launch the browser
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		
	}

  @Test
  public void Run() throws InterruptedException {
		//Launch URL 
		driver.get("https://www.flipkart.com/");
		//close the login popup
		driver.findElementByXPath("//button[text()='✕']").click();
		Actions act=new Actions(driver);
		//mouse hover on Electronics
		WebElement eleElectronics=driver.findElementByXPath("//span[text()='Electronics']");
		act.moveToElement(eleElectronics).build().perform();
		//Click on Mi link
		driver.findElementByLinkText("Mi").click();
		Thread.sleep(2000);
		System.out.println("Current Title "+driver.getTitle());
		//click on Newest first Link
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(3000);
		//Product Names 
		List<WebElement> ele=driver.findElementsByXPath("//div[@class='_3wU53n']");
		List<WebElement> eleprice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		System.out.println(ele.size());
		for (int i = 0; i <ele.size(); i++) {
			System.out.println(ele.get(i).getText());
			System.out.println(eleprice.get(i).getText());
			
		}
		//click on the first resulting Mobile
		String text = ele.get(0).getText();
		ele.get(0).click();
		Thread.sleep(2000);
		Set<String> allWindowHandles = driver.getWindowHandles();
		List<String> allHandles = new ArrayList<>();
		allHandles.addAll(allWindowHandles);
		
		driver.switchTo().window(allHandles.get(allHandles.size()-1));

		System.out.println("Current Title is "+driver.getTitle());
		if(driver.getTitle().contains(text)) {
			System.out.println("Title contains "+text);
		}
		else {
			System.out.println("Title not contains "+ text);
		}
			WebElement eleRatings=driver.findElementByXPath("//div[text()='Ratings & Reviews']/following::span[2]");
			System.out.println("Number of Ratings "+eleRatings.getText().replaceAll("[^-?0-9]+", " "));
			WebElement eleReviews=driver.findElementByXPath("//div[text()='Ratings & Reviews']/following::span[3]");
			System.out.println("Number of Reviews "+eleReviews.getText().replaceAll("[^-?0-9]+", " ")); 
		//close Browser	
			driver.quit();
  }
}
