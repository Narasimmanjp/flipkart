package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class FirstMobileModelPage extends ProjectMethods{

	public FirstMobileModelPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public FirstMobileModelPage getCurrentTitle(String newValue) {
		verifyTitle(newValue);
		return this;
	}
		public FirstMobileModelPage switchToNewWindow() {
			switchToWindow(1);
			return this;
		}
		@FindBy(how=How.XPATH,using="//div[text()='Ratings & Reviews']/following::span[2]")
		WebElement eleRatings;
		@FindBy(how=How.XPATH,using="//div[text()='Ratings & Reviews']/following::span[3]")
		WebElement eleReviews;
		
		
	public FirstMobileModelPage getNumberOfRatings() {
		System.out.println("Number of Ratings "+eleRatings.getText().replaceAll("[^-?0-9]+", " "));
		return this;
	}
	
	public FirstMobileModelPage getNumberOfReviews() {
		System.out.println("Number of Reviews "+eleReviews.getText().replaceAll("[^-?0-9]+", " ")); 
		return this;
	}
	public FirstMobileModelPage closeOpenedBrowser() {
		closeAllBrowsers();
		return this;
	}
	
}
