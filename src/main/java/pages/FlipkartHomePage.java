package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class FlipkartHomePage extends ProjectMethods{

	public FlipkartHomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="//span[text()='Electronics']")
	WebElement eleElectronics;
		
	
	
	public FlipkartHomePage closePopup() {
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		return this;
	}
	
	public FlipkartHomePage  mousehoveronElectronics() {
	movetoElement(eleElectronics);
	return this;
	}
	@FindBy(how=How.LINK_TEXT, using="Mi")
	WebElement eleMiMobileBrand;
	
	public MobileBrandPage clickOnMobileBrandLink() {
		click(eleMiMobileBrand);
		return new MobileBrandPage();
	}
	
}
