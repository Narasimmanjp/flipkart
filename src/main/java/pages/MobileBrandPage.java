package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MobileBrandPage extends ProjectMethods{

	public MobileBrandPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="//div[text()='Newest First']")
	WebElement eleNewestFirst;
	
	public MobileBrandPage getCurrentTitle() {
		System.out.println(getTitle());
		return this;
	}
	public MobileBrandPage clickNewestFirst() {
		click(eleNewestFirst);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//div[@class='_3wU53n']")
	List<WebElement> phoneModels;
		
	
	public MobileBrandPage getAllPhoneModelsNameWithPrice() {
	/*	for (WebElement eachBrand : phoneModels) {
			System.out.println(eachBrand.getText());
		}
	*/	for(int i=0 ;i<phoneModels.size();i++) {
			System.out.println(phoneModels.get(i).getText() +" Price is  "+phonePrice.get(i).getText());
			//System.out.println(phonePrice.get(i).getText());
		}
		return this;
	}
	String FirstMobileModel;
	public String getFirstMobileModelName() {
		FirstMobileModel=phoneModels.get(0).getText();
		System.out.println("FirstMobileModel Name is "+FirstMobileModel);		
		return FirstMobileModel;
	}
	public FirstMobileModelPage clickonFirstListedMobile() {
		click(phoneModels.get(0));
		return new FirstMobileModelPage();
	}
	
	@FindBy(how=How.XPATH,using="//div[@class='_1vC4OE _2rQ-NK']")
	List<WebElement> phonePrice;
	
	public MobileBrandPage getAllPhoneModelPrice() {
		for (WebElement eachBrand : phonePrice) {
			System.out.println(eachBrand.getText());
			}
		return this;
	}
	
	
	
}
