package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//label[text()='Lead ID:']/following::input")
	WebElement eleLeadID;
	@And("Enter the LeadId as (.*)")
	public FindLeadPage enterLeadId(String data) {		
		type(eleLeadID, data);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "(//label[text()='First name:'])[3]/following::input")
	WebElement eleFirstName;
	@And("Enter the firstName as (.*)")
	public FindLeadPage enterFirstName(String data) {		
		type(eleFirstName, data);
		return this;
	}
	@And("Enter the new First Name as (.*)")
	public FindLeadPage enterFirstName1(String data) {		
		type(eleFirstName, data);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "(//label[text()='Last name:'])[3]/following::input")
	WebElement eleLastName;
	@And("Enter the lastName as (.*)")
	public FindLeadPage enterLastName(String data) {		
		type(eleLastName, data);
		return this;
	}

	
	@FindBy(how = How.XPATH, using = "(//label[text()='Company Name:'])[2]/following::input")
	WebElement eleCompanyName;
	@And("Enter the companyName as (.*)")
	
	public FindLeadPage enterCompanyName(String data) {		
		type(eleCompanyName, data);
		return this;
	}
	
		
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement eleFindLead;
	@And("Click Find Lead")
	public FindLeadPage clickFindLead() {		
		click(eleFindLead);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstLead;
	@And("Click First Resulting Lead")
	public ViewLeadPage clickFirstResultingLead() throws InterruptedException {
		Thread.sleep(3000);
		click(eleFirstLead);
		return new ViewLeadPage();
	}
		
	
}
