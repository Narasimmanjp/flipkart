package testcases;

import java.awt.Desktop.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.RegEx;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FlipkartHomePage;
import pages.MobileBrandPage;
import wdMethods.ProjectMethods;

public class TC002WithPages extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_Mi_Mobile";
		testDescription = "Flipkart Mi Mobile Ratings and reviews";
		authors = "Narasimman";
		category = "smoke";
		dataSheetName = "TC002_Flipkart";
		testNodes = "Leads";
	}
  @Test
  public void Run() throws InterruptedException {
	 String newValue=  new FlipkartHomePage()
	  .closePopup()
	  .mousehoveronElectronics()
	  .clickOnMobileBrandLink()
	  .getCurrentTitle()
	  .clickNewestFirst()
	  .getAllPhoneModelsNameWithPrice()
	  .getFirstMobileModelName();
	 new MobileBrandPage()
	 .clickonFirstListedMobile()
	  .switchToNewWindow()
	  .getCurrentTitle(newValue)
	  .getNumberOfRatings()
	  .getNumberOfReviews()
	  .closeOpenedBrowser()
	  ;
		
/*		System.out.println("Current Title "+driver.getTitle());
		//click on Newest first Link
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(3000);
		//Product Names 
		List<WebElement> ele=driver.findElementsByXPath("//div[@class='_3wU53n']");
		List<WebElement> eleprice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		System.out.println(ele.size());
		for (int i = 0; i <ele.size(); i++) {
			System.out.println(ele.get(i).getText());
			System.out.println(eleprice.get(i).getText());
			
		}
		//click on the first resulting Mobile
		String text = ele.get(0).getText();
		ele.get(0).click();
		Thread.sleep(2000);
		Set<String> allWindowHandles = driver.getWindowHandles();
		List<String> allHandles = new ArrayList<>();
		allHandles.addAll(allWindowHandles);
		
		driver.switchTo().window(allHandles.get(allHandles.size()-1));

		System.out.println("Current Title is "+driver.getTitle());
		if(driver.getTitle().contains(text)) {
			System.out.println("Title contains "+text);
		}
		else {
			System.out.println("Title not contains "+ text);
		}
			WebElement eleRatings=driver.findElementByXPath("//div[text()='Ratings & Reviews']/following::span[2]");
			System.out.println("Number of Ratings "+eleRatings.getText().replaceAll("[^-?0-9]+", " "));
			WebElement eleReviews=driver.findElementByXPath("//div[text()='Ratings & Reviews']/following::span[3]");
			System.out.println("Number of Reviews "+eleReviews.getText().replaceAll("[^-?0-9]+", " ")); 
		//close Browser	
			driver.quit();
*/  }
}
